package extra.types;

public class TRef implements IType {

	private IType type;
	
	public TRef(IType type) {
		this.type = type;
	}
	
	public IType getType() {
		return type;
	}
}
