package extra.exception;

public class DuplicateException extends Exception {
	
	private static final long serialVersionUID = 7797615068709177861L;
	
	private String id;
	
	public DuplicateException(String id) {
		this.setId(id);
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
