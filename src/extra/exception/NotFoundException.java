package extra.exception;

public class NotFoundException extends Exception {
	
	private static final long serialVersionUID = -8482176284175035707L;
	
private String id;
	
	public NotFoundException(String id) {
		this.setId(id);
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
