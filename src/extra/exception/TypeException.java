package extra.exception;

public class TypeException extends Exception {
	

	private static final long serialVersionUID = -5239141591930217184L;
	
	public TypeException() {
	}
	
	public TypeException(String string) {
		super(string);
	}
	
}
