package extra;

import extra.types.IType;

public class Param {
	
	String var;
	IType type;

	public Param(String var , IType type) {
		this.var = var;
		this.type = type;
	}

	public String getId() {
		return var;
	}

	public IType getType() {
		return type;
	}
}
