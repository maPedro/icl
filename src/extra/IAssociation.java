package extra;

public interface IAssociation<V> {

	public String getId();

	public void setId(String id);

	public V getValue();

	public void setValue(V value);
}
