package extra;

public class Association<V> implements IAssociation<V> {

	String id;
	V value;

	public Association(String id, V value) {
		this.id = id;
		this.value = value;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public V getValue(){
		return value;
	}

	public void setValue(V value) {
		this.value = value;
	}
}
