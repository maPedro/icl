package extra;

import extra.values.IValue;
import extra.values.VMemory;
import extra.values.VRef;

public class Memory implements IMemory {
	
	public static final Memory singleton = new Memory();

	@Override
	public VRef newRef(IValue value) {
		return new VMemory(value);
	}

	@Override
	public IValue get(VRef ref) {
		return ((VMemory) ref).value;
	}

	@Override
	public IValue set(VRef ref, IValue value) {
		return ((VMemory) ref).value = value;
	}

}
