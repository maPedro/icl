package extra;

import java.util.ArrayList;

import extra.exception.DuplicateException;

public class EnvironmentImpl<V> implements Environment<V> {

	Environment<V> env;
	ArrayList<Association<V>> associations;

	public EnvironmentImpl() {
		this.env = null;
		this.associations = new ArrayList<>();
	}

	public EnvironmentImpl(EnvironmentImpl<V> env) {
		this.env = env;
		this.associations = new ArrayList<>();
	}

	public Environment<V> beginScope() {
		return new EnvironmentImpl<V>(this);
	}

	public Environment<V> endScope() {
		return env;
	}

	public V find(String id) {
		Environment<V> current = this;
		while(current != null){
			for(IAssociation<V> association : this.associations){
				if(association.getId().equals(id)){
					return association.getValue();
				}
			}
			current = current.getEnv();
		}
		return null;
	}

	public void assoc(String id, V value) throws DuplicateException {
		for(IAssociation<V> association : this.associations){
				if(association.getId().equals(id)){
					throw new DuplicateException("duplicate id in environment");
				}
			}
		associations.add(new Association<V>(id,value));
	}
	
	public Environment<V> getEnv() {
		return env;
	}

	public void setEnv(Environment<V> env) {
		this.env = env;
	}

	public ArrayList<Association<V>> getAssociations() {
		return associations;
	}

	public void setAssociations(ArrayList<Association<V>> associations) {
		this.associations = associations;
	}

}
