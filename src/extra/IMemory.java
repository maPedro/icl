package extra;

import extra.values.IValue;
import extra.values.VRef;

public interface IMemory {
	
	VRef newRef(IValue value);
	
	IValue get(VRef ref);
	
	IValue set(VRef ref, IValue value);
}
