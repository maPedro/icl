package extra;

import java.util.ArrayList;

import extra.exception.DuplicateException;
import extra.exception.NotFoundException;

public interface Environment<V> {

	public Environment<V> beginScope();

	public Environment<V> endScope();

	public V find(String id) throws NotFoundException;

	public void assoc(String id, V value) throws DuplicateException;
	
	public Environment<V> getEnv();

	public void setEnv(Environment<V> env);

	public ArrayList<Association<V>> getAssociations();

	public void setAssociations(ArrayList<Association<V>> associations);
}
