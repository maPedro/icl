package extra.values;

public class VInt implements IValue {

	private int value;

	public int getValue() {
		return value;
	}

	public VInt(int value) {
		this.value = value;
	}
	
	@Override
	public boolean equals(Object obj) {
		return obj instanceof VInt && value == ((VInt)obj).getValue();
	}
	
	@Override
	public String toString() {
		return Integer.toString(value);
	}
	
}
