package extra.values;

public class VBool implements IValue {

	private boolean value;

	public boolean isValue() {
		return value;
	}

	public VBool(boolean value) {
		this.value = value;
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof VBool && value == ((VBool) obj).isValue();
	}
	
	@Override
	public String toString() {
		return Boolean.toString(value);
	}

}
