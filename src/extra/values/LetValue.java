package extra.values;

import java.util.List;

import ast.ASTNode;
import extra.Environment;
import extra.Param;

public class LetValue implements IValue{
	
	ASTNode value;
	List<Param> params;
	Environment<IValue> env;
	
	public LetValue(ASTNode value,List<Param> params, Environment<IValue> env) {
		this.value = value;
		this.params = params;
		this.env = env;
	}
	
}
