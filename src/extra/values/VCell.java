package extra.values;

public class VCell implements IValue {

	private IValue value;

	public IValue getValue() {
		return value;
	}

	public VCell(IValue value) {
		this.value = value;
	}
	
	@Override
	public boolean equals(Object obj) {
		return obj instanceof VCell && value == ((VCell)obj).getValue();
	}
	
	@Override
	public String toString() {
		return value.toString();
	}
	
}
