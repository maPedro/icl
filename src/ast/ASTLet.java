package ast;

import java.util.ArrayList;

import extra.Environment;
import extra.EnvironmentImpl;
import extra.exception.DuplicateException;
import extra.exception.NotFoundException;
import extra.exception.TypeException;
import extra.types.IType;
import extra.values.IValue;

public class ASTLet implements ASTNode {

	static class Association {

		String id;
		ASTNode value;
		IType type;

		public Association(String id, ASTNode value) {
			this.id = id;
			this.value = value;
		}
		
		public IType getType() {
			return type;
		}
	}
	ASTNode n1;

	ArrayList<Association> assocs;
	
	IType type;
	
	public ASTLet() {
		assocs = new ArrayList<>();
	}

	public void addBody(ASTNode n1) {
		this.n1 = n1;
	}


	public void newAssociation(String id, ASTNode n){
		assocs.add(new Association(id, n));
	}

	@Override
	public IValue eval(Environment<IValue> env) throws DuplicateException, NotFoundException {

		Environment<IValue> newEnv = env.beginScope();

		for (Association assoc: assocs) {
			newEnv.assoc(assoc.id,assoc.value.eval(env));
		}

		IValue v1 = n1.eval((EnvironmentImpl<IValue>) newEnv);

		env.endScope();

		return v1;
	}

	@Override
	public IType getType() {
		return type;
	}

	@Override
	public IType typecheck(Environment<IType> env) throws TypeException, DuplicateException, NotFoundException {
		Environment<IType> newEnv = env.beginScope();
		
		for (Association assoc: assocs) {
			newEnv.assoc(assoc.id,assoc.type = assoc.value.typecheck(newEnv));
		}
		return null;
	}
	
}
