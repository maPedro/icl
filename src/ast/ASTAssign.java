package ast;

import extra.Environment;
import extra.Memory;
import extra.exception.DuplicateException;
import extra.exception.NotFoundException;
import extra.exception.TypeException;
import extra.types.IType;
import extra.types.TRef;
import extra.values.IValue;
import extra.values.VRef;

public class ASTAssign implements ASTNode {

	ASTNode n1, n2;
	IType type;

	public ASTAssign(ASTNode n1, ASTNode n2) {
		this.n1 = n1;
		this.n2 = n2;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws DuplicateException, NotFoundException {
		IValue v1 = n1.eval(env);
		IValue v2 = n2.eval(env);
		return Memory.singleton.set((VRef) v1, v2);
	}

	@Override
	public IType getType() {
		return type;
	}

	@Override
	public IType typecheck(Environment<IType> env) throws TypeException, DuplicateException, NotFoundException {
		IType t1 = n1.typecheck(env);
		IType t2 = n2.typecheck(env);

		if (t1 instanceof TRef && t2.equals(((TRef) t1).getType())) {
			return (type = t2);
		} else {
			throw new TypeException("Wrong types in assign");
		}
	}

}
