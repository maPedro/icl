package ast;

import extra.Environment;
import extra.Memory;
import extra.exception.DuplicateException;
import extra.exception.NotFoundException;
import extra.exception.TypeException;
import extra.types.IType;
import extra.types.TRef;
import extra.values.IValue;

public class ASTRef implements ASTNode {
	
	ASTNode n;
	IType type;
	
	public ASTRef(ASTNode n) {
		this.n = n;
	}
	@Override
	public IValue eval(Environment<IValue> env) throws DuplicateException, NotFoundException {
		IValue v = n.eval(env);
		return Memory.singleton.newRef(v);
	}

	@Override
	public IType getType() {
		return type;
	}

	@Override
	public IType typecheck(Environment<IType> env) throws TypeException, DuplicateException, NotFoundException {
		IType t = n.typecheck(env);
		type = new TRef(type);
		return type;
	}

}
