package ast;

import extra.Environment;
import extra.exception.DuplicateException;
import extra.exception.NotFoundException;
import extra.exception.TypeException;
import extra.types.IType;
import extra.values.IValue;

public class ASTSeq implements ASTNode {
	
	ASTNode n1;
	ASTNode n2;
	IType type;
	
	public ASTSeq(ASTNode t1, ASTNode t2) {
		this.n1 = t1;
		this.n2 = t2;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws DuplicateException, NotFoundException {
		n1.eval(env);
		return n2.eval(env);
	}
	
	@Override
	public IType getType() {
		return type;
	}
	
	@Override
	public IType typecheck(Environment<IType> env) throws TypeException, DuplicateException, NotFoundException {
		IType t1 = n1.typecheck(env);
		IType t2 = n2.typecheck(env);
		
		if(t2 != null) {
			type = t2;
			return type;
		}
		else
			throw new TypeException("Wrong types in seq");
	}
}
	
