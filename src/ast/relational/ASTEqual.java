package ast.relational;

import ast.ASTNode;
import extra.Environment;
import extra.exception.DuplicateException;
import extra.exception.NotFoundException;
import extra.exception.TypeException;
import extra.types.TBool;
import extra.types.IType;
import extra.types.TInt;
import extra.values.VBool;
import extra.values.IValue;
import extra.values.VInt;

public class ASTEqual implements ASTNode {
	ASTNode n1;
	ASTNode n2;
	
	IType type;
	
	public ASTEqual(ASTNode n1, ASTNode n2) {
		this.n1 = n1;
		this.n2 = n2;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws DuplicateException, NotFoundException {
		IValue v1 = n1.eval(env);
		IValue v2 = n2.eval(env);
		
		if (v1 instanceof VInt && v2 instanceof VInt){
			return new VBool(((VInt)v1).getValue() == ((VInt)v2).getValue());
		} else if (v1 instanceof VBool && v2 instanceof VBool){
			return new VBool(((VBool)v1).isValue() == ((VBool)v2).isValue());
		} else {
			return null;
		}
	}

	@Override
	public IType getType() {
		return type;
	}
	
	@Override
	public IType typecheck(Environment<IType> env) throws TypeException, DuplicateException, NotFoundException {
		IType t1 = n1.typecheck(env);
		IType t2 = n2.typecheck(env);
		
		if(t1 == TInt.singleton && t2 == TInt.singleton) {
			type = TInt.singleton;
			return type;
		} else if(t1 == TBool.singleton && t2 == TBool.singleton) {
			type = TBool.singleton;
			return type;
		}
		else
			throw new TypeException("Wrong types in +");
	}
	
}
