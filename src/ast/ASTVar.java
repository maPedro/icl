package ast;

import extra.Environment;
import extra.exception.DuplicateException;
import extra.exception.NotFoundException;
import extra.exception.TypeException;
import extra.types.IType;
import extra.values.IValue;

public class ASTVar implements ASTNode {
	
	String var;
	IType type;
	
	public ASTVar(String var) {
		this.var = var;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws DuplicateException, NotFoundException {
		return env.find(var);
	}

	@Override
	public IType getType() {
		return type;
	}

	@Override
	public IType typecheck(Environment<IType> env) throws TypeException, DuplicateException, NotFoundException {
		type = env.find(var);
		return type;
	}

}
