package ast.logical;

import ast.ASTNode;
import extra.Environment;
import extra.exception.DuplicateException;
import extra.exception.NotFoundException;
import extra.exception.TypeException;
import extra.types.TBool;
import extra.types.IType;
import extra.values.VBool;
import extra.values.IValue;

public class ASTOr implements ASTNode {
	ASTNode n1;
	ASTNode n2;
	IType type;
	
	public ASTOr(ASTNode n1, ASTNode n2) {
		this.n1 = n1;
		this.n2 = n2;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws DuplicateException, NotFoundException {
		IValue v1 = n1.eval(env);
		IValue v2 = n2.eval(env);
		
		if (v1 instanceof VBool && v2 instanceof VBool){
			return new VBool(((VBool)n1.eval(env)).isValue() || ((VBool)n2.eval(env)).isValue());
		} else {
			return null;
		}
	}
	
	@Override
	public IType getType() {
		return type;
	}
	
	@Override
	public IType typecheck(Environment<IType> env) throws TypeException, DuplicateException, NotFoundException {
		IType t1 = n1.typecheck(env);
		IType t2 = n2.typecheck(env);
		
		if (t1 == TBool.singleton || t2 == TBool.singleton) {
			type = TBool.singleton;
			return type;
		}
		else throw new TypeException("Wrong type in And");
	}
	
}
