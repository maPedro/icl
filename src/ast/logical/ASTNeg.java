package ast.logical;

import ast.ASTNode;
import extra.Environment;
import extra.exception.DuplicateException;
import extra.exception.NotFoundException;
import extra.exception.TypeException;
import extra.types.IType;
import extra.types.TInt;
import extra.values.IValue;
import extra.values.VInt;

public class ASTNeg implements ASTNode {
	ASTNode n1;
	IType type;
	
	public ASTNeg(ASTNode n1) {
		this.n1 = n1;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws DuplicateException, NotFoundException {
		IValue v1 = n1.eval(env);
		
		if(v1 instanceof VInt) {
			return new VInt(0 - ((VInt)v1).getValue());
		} else {
			return null;
		}
	}
	
	@Override
	public IType getType() {
		return type;
	}
	
	@Override
	public IType typecheck(Environment<IType> env) throws TypeException, DuplicateException, NotFoundException {
		IType t1 = n1.typecheck(env);
		
		if(t1 == TInt.singleton) {
			type = TInt.singleton;
			return type;
		}
		else
			throw new TypeException("Wrong types in ~");
	}
	
	@Override
	public String toString() {
		return "~" + n1.toString();
	}
	
}
