package ast;

import extra.Environment;
import extra.exception.DuplicateException;
import extra.exception.NotFoundException;
import extra.exception.TypeException;
import extra.types.IType;
import extra.values.IValue;

public interface ASTNode {
	
	public IValue eval(Environment<IValue> env) throws DuplicateException, NotFoundException;

	public IType getType();
	
	public IType typecheck(Environment<IType> env) throws TypeException, DuplicateException, NotFoundException;
}
