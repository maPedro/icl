package ast;

import extra.Environment;
import extra.exception.DuplicateException;
import extra.exception.NotFoundException;
import extra.exception.TypeException;
import extra.types.IType;
import extra.types.TBool;
import extra.values.IValue;
import extra.values.VBool;

public class ASTWhile implements ASTNode {
	
	ASTNode n1, n2;
	IType type;
	
	public ASTWhile(ASTNode n1, ASTNode n2) {
		this.n1 = n1;
		this.n2 = n2;
	}

	@Override
	public IValue eval(Environment<IValue> env) throws DuplicateException, NotFoundException {
		
		while(((VBool)n1.eval(env)).isValue()) {
			n2.eval(env);
		}
		return new VBool(false);
	}

	@Override
	public IType getType() {
		return type;
	}

	@Override
	public IType typecheck(Environment<IType> env) throws TypeException, DuplicateException, NotFoundException {
		IType t1 = n1.typecheck(env);
		IType t2 = n2.typecheck(env);
		
		if(t1 != TBool.singleton || t2 == null) {
			throw new TypeException("Wrong type in while");
		} else {
			type = TBool.singleton;
			return type;
		}
	}

}
