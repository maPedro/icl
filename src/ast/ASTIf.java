package ast;

import extra.Environment;
import extra.exception.DuplicateException;
import extra.exception.NotFoundException;
import extra.exception.TypeException;
import extra.types.IType;
import extra.types.TBool;
import extra.values.IValue;
import extra.values.VBool;

public class ASTIf implements ASTNode {
	
	ASTNode n1,n2,n3;
	IType type;
	
	public ASTIf(ASTNode n1, ASTNode n2, ASTNode n3) {
		this.n1 = n1;
		this.n2 = n2;
		this.n3 = n3;
	}
	@Override
	public IValue eval(Environment<IValue> env) throws DuplicateException, NotFoundException {
		IValue v1 = n1.eval(env);
		IValue v2;
		
		if(((VBool) v1).isValue() == true) {
			v2 = n2.eval(env);
		} else {
			v2 = n3.eval(env);
		}

		return v2;
	}

	@Override
	public IType getType() {
		return type;
	}

	@Override
	public IType typecheck(Environment<IType> env) throws TypeException, DuplicateException, NotFoundException {
		IType t1 = n1.typecheck(env);
		IType t2 = n2.typecheck(env);
		IType t3 = n3.typecheck(env);
		
		if(t1 != TBool.singleton || t2==null || t3==null || t2!=t3) {
			throw new TypeException("Wrong types in if");
		} else{
			return(type = t2);
		}
	}

}
