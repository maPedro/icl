package ast;

import extra.Environment;
import extra.exception.TypeException;
import extra.types.IType;
import extra.types.TInt;
import extra.values.IValue;
import extra.values.VInt;

public class ASTNum implements ASTNode {
	
	int num;
	
	public ASTNum(int parseInt) {
		this.num = parseInt;
	}

	public IValue eval (Environment<IValue> env) {
		return new VInt(num);
	}
	
	@Override
	public IType getType() {
		return TInt.singleton;
	}
	
	@Override
	public IType typecheck(Environment<IType> env) throws TypeException {
		return TInt.singleton;
	}
	
	@Override
	public String toString() {
		return Integer.toString(num);
	}
}
