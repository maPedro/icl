package ast;

import extra.Environment;
import extra.exception.TypeException;
import extra.types.IType;
import extra.types.TBool;
import extra.values.IValue;
import extra.values.VBool;

public class ASTBoolean implements ASTNode {
	
	boolean value;
	
	public ASTBoolean(boolean value) {
		this.value = value;
	}
	
	@Override
	public IValue eval(Environment<IValue> env) {
		return new VBool(value);
	}
	
	@Override
	public IType typecheck(Environment<IType> env) throws TypeException {
		return TBool.singleton;
	}

	@Override
	public IType getType() {
		return TBool.singleton;
	}
	
	@Override
	public String toString() {
		return Boolean.toString(value);
	}
}
